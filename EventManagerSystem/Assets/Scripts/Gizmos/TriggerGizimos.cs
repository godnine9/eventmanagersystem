﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerGizimos : MonoBehaviour
{
    public bool IsActiveable          = true;
    public Color OutLineColor         = new Color(255, 255, 255, 0.3f);
    public Color ActiveColor          = new Color(0  , 255, 0  , 0.15f);
    public Color SelectedOutlineColor = new Color(255, 150, 0  , 0.3f);
    public Color InActiveColor        = new Color(255,   0, 0  , 0.15f);

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        if (IsActiveable)
        {
            DrawColliders(this, ActiveColor, OutLineColor);
        }
        {
            DrawColliders(this, InActiveColor, OutLineColor);
        }
    }

    void OnDrawGizmosSelected()
    {
        DrawColliders(this, SelectedOutlineColor, OutLineColor , true);
    }
#endif

    public static void DrawColliders(MonoBehaviour mb, Color iBaseColor, bool iIsSelected = false)
    {
#if UNITY_EDITOR
        DrawColliders(mb, new Color(iBaseColor.r, iBaseColor.g, iBaseColor.b, 0.25f), new Color(iBaseColor.r, iBaseColor.g, iBaseColor.b, 1.0f), iIsSelected);
#endif
    }

    public static void DrawColliders(MonoBehaviour mb, Color meshColor, Color outlineColor, bool bSelected = false)
    {
#if UNITY_EDITOR
        if (bSelected)
        {
            Vector3 DisPlayPosition = mb.transform.position + mb.transform.up * 5;
            DebugDrawText.DrawText(DisPlayPosition, mb.name, new Color(1, 1, 1, 1), new Color(0, 0, 0, 0.7f));
        }
        else
        {
            outlineColor.a *= 0.5f;
        }

        Collider[] colliders = (mb.GetComponent<Rigidbody>()) ? mb.GetComponentsInChildren<Collider>() : mb.GetComponents<Collider>();

        foreach (Collider aCollider in colliders)
        {
            Gizmos.matrix = aCollider.transform.localToWorldMatrix;
            Gizmos.color = meshColor;

            if (aCollider.GetType() == typeof(BoxCollider))
            {
                BoxCollider boxCollider = (BoxCollider)aCollider;

                Gizmos.DrawCube(boxCollider.center, boxCollider.size);

                Gizmos.color = outlineColor;
                Gizmos.DrawWireCube(boxCollider.center, boxCollider.size);
            }
            else if (aCollider.GetType() == typeof(CapsuleCollider))
            {
                CapsuleCollider capsuleCollider = (CapsuleCollider)aCollider;
                float yOffset = (0.5f * capsuleCollider.height - capsuleCollider.radius);

                Gizmos.DrawSphere(capsuleCollider.center + Vector3.up * yOffset, capsuleCollider.radius);
                Gizmos.DrawSphere(capsuleCollider.center - Vector3.up * yOffset, capsuleCollider.radius);

                Gizmos.color = outlineColor;
                Gizmos.DrawWireSphere(capsuleCollider.center + Vector3.up * yOffset, capsuleCollider.radius);
                Gizmos.DrawWireSphere(capsuleCollider.center - Vector3.up * yOffset, capsuleCollider.radius);
            }
            else if (aCollider.GetType() == typeof(SphereCollider))
            {
                SphereCollider sphereCollider = (SphereCollider)aCollider;

                Gizmos.DrawSphere(sphereCollider.center, sphereCollider.radius);

                Gizmos.color = outlineColor;
                Gizmos.DrawWireSphere(sphereCollider.center, sphereCollider.radius);
            }
            else if (aCollider.GetType() == typeof(MeshCollider))
            {
                MeshCollider meshCollider = (MeshCollider)aCollider;

                Gizmos.DrawMesh(meshCollider.sharedMesh);

                Gizmos.color = outlineColor;
                Gizmos.DrawWireMesh(meshCollider.sharedMesh);
            }
        }
#endif
    }
}
