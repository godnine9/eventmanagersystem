using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
public class EventLogCollection
{
    static public EventLogger Logger
    {
        set
        {
            Instance.logger = value;
        }
    }

    static public bool Dirty
    {
        get { return Instance.dirty; }
        set { Instance.dirty = value; }
    }

    static public void AddEntryForSource(EventMessage message, UnityEngine.Object source)
    {
        Instance.logger.AddSourceEntry(message, source);
        Instance.dirty = true;
    }

    static public void AddEntryForListener(EventMessage message, UnityEngine.Object listener)
    {
        Instance.logger.AddListenerEntry(message, listener);
        Instance.dirty = true;
    }

    static public void Clear()
    {
        Instance.logger.Clear();
        Instance.dirty = true;
    }

    static public ReadOnlyCollection<string> GetEntries()
    {
        return Instance.logger.GetCollection().Get();
    }

    static public ReadOnlyCollection<string> FilterBy(string[] keywords)
    {
        Func<string, bool> f = (string x) =>
        {
            for (int i = 0, size = keywords.Length; i < size; ++i)
            {
                if (x.Contains(keywords[i]) == false)
                {
                    return false;
                }
            }
            return true;
        };

        return Instance.logger.GetCollection().Find(f);
    }

    static public UnityEngine.Object GetObject(string text)
    {
        EventLogger.EntryDetail ed = Instance.logger.GetDetail(text);
        if (ed.listener != null)
        {
            return ed.listener;
        }
        else
        {
            return ed.source;
        }
    }

    static public EventLogger.EntryDetail GetDetail(string text)
    {
        return Instance.logger.GetDetail(text);
    }

    static public bool IsSourceEntry(string text)
    {
        return Instance.logger.IsSourceEntry(text);
    }


    #region Private

    private EventLogger logger = new EmptyEventLogger();
    private bool dirty = false;

    #endregion

    #region Singleton Shits

    private static readonly EventLogCollection instance = new EventLogCollection();

    private EventLogCollection()
    {

    }

    private static EventLogCollection Instance
    {
        get
        {
            return instance;
        }
    }

    #endregion
}