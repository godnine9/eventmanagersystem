using UnityEngine;
using System.Collections;

public class SingleEventSource : MonoBehaviour
{
    public EventArray sendEvent;

    [ContextMenu("SendEvent ()")]
    public void SendEvent()
    {
#if UNITY_EDITOR
        if (pauseHere)
        {
            StartCoroutine(WaitForUnpause());
        }
        else
#endif
        {
            SendEventInternal();
        }
    }

    private void SendEventInternal()
    {
        sendEvent.Broadcast(gameObject, this);
    }

#if UNITY_EDITOR

    public bool pauseHere = false;

    private IEnumerator WaitForUnpause()
    {
        Debug.Break();

        while (UnityEditor.EditorApplication.isPaused)
        {
            yield return new WaitForEndOfFrame();
        }

        SendEventInternal();
    }

#endif
}