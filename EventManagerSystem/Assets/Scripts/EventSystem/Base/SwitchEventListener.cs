using UnityEngine;
using System.Collections;

public class SwitchEventListener : ConditionEventListener
{
    public bool useParamBool = false;
    public bool inverse = false;

    private bool tmpMessageBool;

    protected override sealed void PreOnEvent(EventMessage message, ref object paramRef)
    {
        tmpMessageBool = message.paramBool;
        message.paramBool = ((useParamBool) ? message.paramBool : true) ^ inverse;
    }

    protected override sealed void PostOnEvent(EventMessage message, ref object paramRef)
    {
        message.paramBool = tmpMessageBool;// avoid to modify by PreOnEvent
    }
}