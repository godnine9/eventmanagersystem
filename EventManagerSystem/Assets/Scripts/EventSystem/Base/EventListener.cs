using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;
using System.Collections.Generic;

public class EventListener : MonoBehaviour
{
    public enum RecieveType
    {
        BOTH,
        GLOBAL,
        LOCAL,
    }

	public List<string> referencePath = new List<string>();

    [FormerlySerializedAs("m_eventID")]
    [EventIdAttribute]
	public string eventID;

    [FormerlySerializedAs("m_recvType")]
    public RecieveType receiveType = RecieveType.BOTH;

    protected virtual void Awake()
    {
        Register();
    }

    protected virtual void OnDestroy()
    {
        Unregister();
    }

    protected virtual bool PassEvent()
    {
        return true;
    }

    protected virtual void PreOnEvent(EventMessage message, ref object paramRef)
    {

    }

    protected virtual void OnEvent(EventMessage message, ref object paramRef)
    {

    }

    protected virtual void PostOnEvent(EventMessage message, ref object paramRef)
    {

    }

        private bool isClean = true;
    private bool origEnabled;
    private bool setOrigEnabled = true;

    public bool IsClean() // Am I without any registry?
    {
        return isClean;
    }

    public void RecoverEnabledFlag()
    {
        enabled = origEnabled;
    }

    public void ForceRegister()
    {
        Register();
    }

    public void OverrideRegister()
    {
        if (!isClean)
        {
            Unregister();
        }
        Register();
    }

    public void TriggerEvent(EventMessage message, ref object obj)
    {
        if(message != null)
        {
            React(message, ref obj);
        }            
    }

    private void Register()
    {
        if (!isClean)
        {
            return;
        }

        // For recycle, restore enabled status
        if (setOrigEnabled)
        {
            origEnabled = enabled;
            setOrigEnabled = false;
        }

        if (!string.IsNullOrEmpty(eventID))
        {
            switch (receiveType)
            {
            case RecieveType.BOTH:
                EventManager.AddEventListener(gameObject, eventID, React);
                break;
            case RecieveType.LOCAL:
                EventManager.AddObjectEventListener(gameObject, eventID, React);
                break;
            case RecieveType.GLOBAL:
                EventManager.AddGlobalEventListener(eventID, React);
                break;
            }
            isClean = false;
        }
    }

    private void Unregister()
    {
        if (!string.IsNullOrEmpty(eventID))
        {
            EventManager.RemoveEventListener(gameObject, eventID, React);
        }
        isClean = true;
    }

    protected void React(EventMessage message, ref object paramRef)
    {
        if(!PassEvent())
        {
            return;
        }

        PreOnEvent(message, ref paramRef);

#if UNITY_EDITOR
        if (pauseHere)
        {
            StartCoroutine(WaitForUnpause(message));
        }
        else
#endif
        {
            ReceiveEvent(message, ref paramRef);
        }

        PostOnEvent(message, ref paramRef);
    }

    private void ReceiveEvent(EventMessage message, ref object paramRef)
    {
        AddEventLog(message);
        OnEvent(message, ref paramRef);
    }

    private void AddEventLog(EventMessage message)
    {
			if (this != null)
			{
				EventLogCollection.AddEntryForListener(message, gameObject);
			}
    }

    #region Editor Only
#if UNITY_EDITOR

    [ContextMenu("Receive (True)")]
    public void SendTrue()
    {
        object paramRef = null;
        EventMessage message = new EventMessage(eventID, this, true);
        React(message, ref paramRef);
    }

    [ContextMenu("Receive (False)")]
	public void SendFalse()
    {
        object paramRef = null;
        EventMessage message = new EventMessage(eventID, this, false);
        React(message, ref paramRef);
    }

    public bool pauseHere = false;

    private IEnumerator WaitForUnpause(EventMessage message)
    {
        Debug.Break();

        while (UnityEditor.EditorApplication.isPaused)
        {
            yield return new WaitForEndOfFrame();
        }

        object o = null;
        ReceiveEvent(message, ref o);
    }

#endif
    #endregion
}