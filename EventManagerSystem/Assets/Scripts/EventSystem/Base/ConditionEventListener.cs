using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

public class ConditionEventListener : EventListener
{
    public string openCondition;
    public string closeCondition;


    public bool CheckCondition(string condition)
    {
        bool result = true;
        bool initResultValue = false;

        if (!initResultValue)
        {
            result = true;
            initResultValue = true;
        }

        return result;
    }

    protected override bool PassEvent()
    {
        if (!string.IsNullOrEmpty(openCondition) && !CheckCondition(openCondition))
        {
            return false;
        }

        if (!string.IsNullOrEmpty(closeCondition) && CheckCondition(closeCondition))
        {
            return false;
        }

        return true;
    }
}