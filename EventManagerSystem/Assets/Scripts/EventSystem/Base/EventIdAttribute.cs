using System;
using UnityEngine;

[AttributeUsage(AttributeTargets.Field, Inherited = false)]
public sealed class EventIdAttribute : PropertyAttribute
{
}

public sealed class SenderEventIdAttribute : PropertyAttribute
{
}
