using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public static class EventManager
{
    #region EventManager static

    // Event table
    static EventManagerBase eventManager = new EventManagerBase();

    // Event condition table
    static Hashtable eventConditionTable = new Hashtable();
    public static Hashtable EventConditionTable
    {
        get { return eventConditionTable; }
        set { eventConditionTable = value; }
    }

    public static void CompleteCondition(string condition)
    {
        if(string.IsNullOrEmpty(condition))
        {
            return;
        }

        if(!eventConditionTable.ContainsKey(condition))
        {
            eventConditionTable.Add(condition, 0);
        }
        else
        {
            eventConditionTable[condition] = System.Convert.ToInt32(eventConditionTable[condition]) + 1;
        }
    }

    public static void ResetCondition(string condition)
    {
        if(string.IsNullOrEmpty(condition))
        {
            return;
        }

        if(eventConditionTable.ContainsKey(condition))
        {
            eventConditionTable.Remove(condition);
        }
    }

    // Singleton EventManager
    static EventManager()
    {
        // Test function
        AddEventListener(null, "Debug", DebugFunc);
    }

    static void DebugFunc(EventMessage message, ref object paramRef)
    {
        string log = string.Format(
            "[EventManager] [{0}], {1}, {2}, [{3}]", 
            message.eventID, message.origSource, message.paramBool, message.paramString);
        if (message.paramEX != null)
        {
            foreach (object obj in message.paramEX)
            {
                log = string.Format("{0}, {1}", log, obj);
            }
        }
        Debug.Log(log);
    }

    #endregion EventManager static

    #region Global Event

    public static void SendGlobalEvent(
        string eventID,
        bool paramBool = true,
        string paramString = null,
        Object origSource = null,
        Vector2 delayTime = default(Vector2),
        bool useTimeScale = true,
        params object[] paramEX
    )
    {
        EventMessage em = new EventMessage(eventID, origSource, paramBool, paramString, paramEX, delayTime, useTimeScale);
        SendGlobalEvent(em);
    }

    public static void SendGlobalEvent(EventMessage em)
    {
        eventManager.Send(em);
    }

    public static void AddGlobalEventListener(string eventID, EventFunction ed)
    {
        if (string.IsNullOrEmpty(eventID))
        {
            return;
        }

        eventManager.AddListener(eventID, ed);
    }

    public static void RemoveGlobalEventListener(string eventID, EventFunction ed)
    {
        if (string.IsNullOrEmpty(eventID))
        {
            return;
        }

        eventManager.RemoveListener(eventID, ed);
    }

    #endregion Global Event

    #region Object Event

    public static void SendObjectEvent(
        GameObject targetObj,
        string eventID,
        bool paramBool = true,
        string paramString = null,
        Object origSource = null,
        Vector2 delayTime = default(Vector2),
        bool useTimeScale = false,
        params object[] paramEX
    )
    {
        EventMessage em = new EventMessage(targetObj, eventID, origSource, paramBool, paramString, paramEX, delayTime, useTimeScale);
        SendObjectEvent(em);
    }

    public static void SendObjectEvent(EventMessage em)
    {
        if (em.targetObj == null)
        {
            return;
        }

        ObjectEventManager.Send(em);
    }

    public static void AddObjectEventListener(GameObject handleObj, string eventID, EventFunction ed)
    {
        if (string.IsNullOrEmpty(eventID))
        {
            return;
        }

        if (handleObj != null)
        {
            ObjectEventManager.AddEventListener(handleObj, eventID, ed);
        }
    }

    public static void RemoveObjectEventListener(GameObject handleObj, string eventID, EventFunction ed)
    {
        if (string.IsNullOrEmpty(eventID))
        {
            return;
        }

        if (handleObj != null)
        {
            ObjectEventManager.RemoveEventListener(handleObj, eventID, ed);
        }
    }

    #endregion Object Event

    public static void AddEventListener(GameObject handleObj, string eventID, EventFunction ed)
    {
        if (string.IsNullOrEmpty(eventID))
        {
            return;
        }

        // Global Event
        AddGlobalEventListener(eventID, ed);

        // Object Event
        AddObjectEventListener(handleObj, eventID, ed);
    }

    public static void RemoveEventListener(GameObject handleObj, string eventID, EventFunction ed)
    {
        if (string.IsNullOrEmpty(eventID))
        {
            return;
        }

        // Global Event
        RemoveGlobalEventListener(eventID, ed);

        // Object Event
        RemoveObjectEventListener(handleObj, eventID, ed);
    }
}