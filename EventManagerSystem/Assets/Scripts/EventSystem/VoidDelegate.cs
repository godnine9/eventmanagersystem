using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

public delegate void VoidDelegate();

public class SortedVoidDelegate
{
    SortedDictionary<int, List<VoidDelegateElement>> dictionary = new SortedDictionary<int, List<VoidDelegateElement>>();
    bool isInvoking = false;

    static List<int> removeKey = new List<int>();
    static List<VoidDelegateElement> removeList = new List<VoidDelegateElement>();

    bool Add(VoidDelegate _callback, object _key, int _order)
    {
        bool result = false;
        List<VoidDelegateElement> currList;

        if (!dictionary.ContainsKey(_order))
        {
            currList = new List<VoidDelegateElement>();
            VoidDelegateElement ele = new VoidDelegateElement(_callback, _key, _order);
            currList.Add(ele);
            dictionary.Add(_order, currList);
        }
        else
        {
            currList = dictionary[_order];

            bool found = false;
            foreach (VoidDelegateElement ele in currList)
            {
                if (ele.callback == _callback && ele.key == _key)
                {
                    // Already exist
                    found = true;
                }
            }

            // TODO: check duplicate item in different order
            if (!found)
            {
                // Add
                VoidDelegateElement ele = new VoidDelegateElement(_callback, _key, _order);
                currList.Add(ele);
            }

        }
        return result;
    }

    bool Remove(VoidDelegate _callback, object _key)
    {
        foreach (KeyValuePair<int, List<VoidDelegateElement>> kvp in dictionary)
        {
            List<VoidDelegateElement> currList = kvp.Value;
            if (currList != null)
            {
                if (Remove(_callback, _key, kvp.Key))
                {
                    return true;
                }
            }
        }
        return false;
    }

    bool Remove(VoidDelegate _callback, object _key, int _order)
    {
        bool result = false;
        if (dictionary.ContainsKey(_order))
        {
            List<VoidDelegateElement> currList = dictionary[_order];

            foreach (VoidDelegateElement ele in currList)
            {
                if (ele.callback == _callback && ele.key == _key)
                {
                    if (!isInvoking)
                    {
                        currList.Remove(ele);
                        result = true;
                        break;
                    }
                    else
                    {
                        // Mark for delete
                        ele.markDeleted = true;
                    }
                }
            }
            if (currList.Count <= 0)
            {
                dictionary.Remove(_order);
            }
        }
        return result;
    }

    bool IsContain(VoidDelegate _callback, object _key, int _order)
    {
        if (dictionary.ContainsKey(_order))
        {
            foreach (VoidDelegateElement ele in dictionary[_order])
            {
                if (ele.callback == _callback && ele.key == _key)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public void Register(VoidDelegate _callback, object _key, int _order)
    {
        Add(_callback, _key, _order);
    }

    public void Unregister(VoidDelegate _callback, object _key, int _order)
    {
        Remove(_callback, _key);
    }

    public void Invoke()
    {
        isInvoking = true;
        removeKey.Clear();

        foreach (KeyValuePair<int, List<VoidDelegateElement>> kvp in dictionary)
        {
            List<VoidDelegateElement> currList = kvp.Value;
            if (currList != null)
            {
                removeList.Clear();

                foreach (VoidDelegateElement ele in currList)
                {
                    // Check key(instance)
                    if (!ele.Invoke())
                    {
                        removeList.Add(ele);
                    }
                }

                foreach (VoidDelegateElement ele in removeList)
                {
                    currList.Remove(ele);
                }

                if (currList.Count <= 0)
                {
                    removeKey.Add(kvp.Key);
                }
            }
            else
            {
                removeKey.Add(kvp.Key);
            }
        }
        foreach (int k in removeKey)
        {
            dictionary.Remove(k);
        }
        isInvoking = false;
    }

    public void Refresh()
    {
        removeKey.Clear();

        foreach (KeyValuePair<int, List<VoidDelegateElement>> kvp in dictionary)
        {
            List<VoidDelegateElement> currList = kvp.Value;
            if (currList != null)
            {
                removeList.Clear();

                foreach (VoidDelegateElement ele in currList)
                {
                    if (!ele.IsValid())
                    {
                        removeList.Add(ele);
                    }
                }

                foreach (VoidDelegateElement ele in removeList)
                {
                    currList.Remove(ele);
                }

                if (currList.Count <= 0)
                {
                    removeKey.Add(kvp.Key);
                }
            }
            else
            {
                removeKey.Add(kvp.Key);
            }
        }
        foreach (int k in removeKey)
        {
            dictionary.Remove(k);
        }
    }
}

public class VoidDelegateElement
{
    public int order;
    public object key;
    public VoidDelegate callback;
    public bool markDeleted = false;

    public VoidDelegateElement(VoidDelegate _callback, object _key, int _order)
    {
        callback = _callback;
        key = _key;
        order = _order;
    }

    public bool IsValid()
    {
        if (markDeleted)
            return false;

        if (callback != null && key != null)
        {
#if !UNITY_EDITOR && UNITY_WSA_10_0___  // shao: il2cpp win 10 using same code...
                if (key.GetType().GetTypeInfo().IsSubclassOf(typeof(UnityEngine.Object)))
                {
                    UnityEngine.Object obj = key as UnityEngine.Object;
                    if (obj != null)
                    {
                        return true;
                    }
                }
                else
                {
					return true;
				}
#else
            if (key.GetType().IsSubclassOf(typeof(UnityEngine.Object)))
            {
                UnityEngine.Object obj = key as UnityEngine.Object;
                if (obj != null)
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
#endif
        }
        return false;
    }

    public bool Invoke()
    {
        if (IsValid())
        {
            UnityEngine.Profiling.Profiler.BeginSample("VoidDelegate: " + this.key.ToString());
            callback();
            UnityEngine.Profiling.Profiler.EndSample();
            return true;
        }
        return false;
    }
}