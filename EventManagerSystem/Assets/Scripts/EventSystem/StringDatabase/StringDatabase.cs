using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StringDatabase
{
    private Dictionary<string, StringCollection> database = new Dictionary<string, StringCollection>();

    public void AddCollection(string key, Parameter parameter)
    {
        database.Add(key, new StringCollection(parameter));
    }

    public void RemoveCollection(string key)
    {
        database.Remove(key);
    }

    public bool HasCollection(string key)
    {
        return database.ContainsKey(key);
    }

    public StringCollection GetCollection(string key)
    {
        StringCollection c = null;
        try
        {
            c = database[key];
        }
        catch
        {
            Debug.LogErrorFormat("{0} is an invalid string collection.", key);
        }
        return c;
    }

    #region Singleton Shits
    //////////////////////////////////////////////////////////////////////////////////////
    static private readonly StringDatabase instance = new StringDatabase();

    private StringDatabase()
    {

    }

    static public StringDatabase Instance
    {
        get
        {
            return instance;
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////
    #endregion
}