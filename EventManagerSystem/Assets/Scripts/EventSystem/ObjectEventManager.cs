using UnityEngine;
using System.Collections;
public class ObjectEventManager : MonoBehaviour
{
    EventManagerBase eventManager = new EventManagerBase();

    static ObjectEventManager GetOrCreateObjectEventManager(GameObject targetObj)
    {
        ObjectEventManager oem = targetObj.GetComponent<ObjectEventManager>();
        if (oem == null)
        {
            if (Application.isPlaying)
            {
                oem = targetObj.AddComponent<ObjectEventManager>();
            }
        }
        return oem;
    }

    public static void AddEventListener(GameObject targetObj, string eventID, EventFunction ed)
    {
        ObjectEventManager oem = GetOrCreateObjectEventManager(targetObj);
        if (oem != null)
        {
            oem.eventManager.AddListener(eventID, ed);
        }
    }

    public static void RemoveEventListener(GameObject targetObj, string eventID, EventFunction ed)
    {
        ObjectEventManager oem = targetObj.GetComponent<ObjectEventManager>();
        if (oem != null)
        {
            oem.eventManager.RemoveListener(eventID, ed);
        }
    }

    public static void Send(EventMessage em)
    {
        // for hierachy object
        ObjectEventManager[] oems = em.targetObj.GetComponentsInChildren<ObjectEventManager>();

        if (oems != null)
        {
            foreach (ObjectEventManager oem in oems)
            {
                oem.eventManager.Send(em);
            }
        }
    }
}
