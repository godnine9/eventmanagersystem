using UnityEngine;
using System.Collections;

namespace BadBird
{
	public class StartToEvent : SingleEventSource
	{
		void Start()
		{
			SendEvent();
		}
	}
}


