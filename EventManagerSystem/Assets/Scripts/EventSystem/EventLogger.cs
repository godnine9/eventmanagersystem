using UnityEngine;

abstract public class EventLogger
{
    public class EntryDetail
    {
        public string eventId;
        public Object source;
        public Object listener;
        public float timeStamp;
    }

    abstract public void Clear();
    abstract public void AddSourceEntry(EventMessage message, UnityEngine.Object source);
    abstract public void AddListenerEntry(EventMessage message, UnityEngine.Object listener);
    abstract public StringCollection GetCollection();
    abstract public EntryDetail GetDetail(string text);
    abstract public bool IsSourceEntry(string text);
}
