using UnityEngine;
using System.Collections;

public class EventTimer : MonoBehaviour
{
    [SerializeField]
    int timerCount = 0;

    class BroadcastInfo
    {
        public EventMessage em;
        public float delayTime;
        public bool useTimeScale;
        public EventFunction sendDelegate;
        public VoidDelegate sendVoidDelegate;
    }

    public static EventTimer Get(GameObject go)
    {
        EventTimer et = go.GetComponent<EventTimer>();
        if (et == null)
        {
            et = go.AddComponent<EventTimer>();
        }
        return et;
    }

    void AddTimerInternal(BroadcastInfo bi)
    {
        // Add one timer
        timerCount++;

        StartCoroutine("Broadcast", bi);
    }

    IEnumerator Broadcast(BroadcastInfo bi)
    {
        if (bi.em != null)
        {
            bi.useTimeScale = bi.em.useTimeScale;
        }

        if (bi.useTimeScale)
        {
            yield return new WaitForSeconds(bi.delayTime);
        }
        else
        {
            float pauseEndTime = Time.realtimeSinceStartup + bi.delayTime;
            while (Time.realtimeSinceStartup < pauseEndTime)
            {
                yield return null;
            }
        }

        if (bi.sendDelegate != null)
        {
            object paramRef = null;
            bi.sendDelegate(bi.em, ref paramRef);
        }
        if (bi.sendVoidDelegate != null)
        {
            bi.sendVoidDelegate();
        }

        // remove timer
        timerCount--;

        if (timerCount <= 0)
        {
            Destroy(this);
        }
    }

    public static void AddTimer(EventMessage em, EventFunction sendDelegate)
    {
        // Get GameObject from Object
        GameObject attachObj = EventElement.GetGameObject(em.origSource);

        BroadcastInfo bi = new BroadcastInfo();
        bi.em = em;
        bi.sendDelegate = sendDelegate;

        // Calculate delay time
        bi.delayTime = Random.Range(Mathf.Min(em.delayTime[0], em.delayTime[1]), Mathf.Max(em.delayTime[0], em.delayTime[1]));

        EventTimer et = EventTimer.Get(attachObj);
        if (et != null)
        {
            et.AddTimerInternal(bi);
        }
    }

    public static void AddTimer(float delayTime, VoidDelegate sendDelegate, Object origSource = null, bool useTimeScale = false)
    {
        // Get GameObject from Object
        GameObject attachObj = EventElement.GetGameObject(origSource);

        BroadcastInfo bi = new BroadcastInfo();
        bi.sendVoidDelegate = sendDelegate;
        bi.delayTime = delayTime;
        bi.useTimeScale = useTimeScale;

        EventTimer et = EventTimer.Get(attachObj);
        if (et != null)
        {
            et.AddTimerInternal(bi);
        }
    }
}