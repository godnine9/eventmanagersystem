public class EventInfo
{
    string eventID
    {
        get;
        set;
    }

    EventFunction onEvent;

    // Constructor
    public EventInfo(string eventID)
    {
        this.eventID = eventID;
    }

    public void AddDelegate(EventFunction ed)
    {
        onEvent += ed;
    }

    public void RemoveDelegate(EventFunction ed)
    {
        onEvent -= ed;
    }

    public void Send(EventMessage em)
    {
        if (onEvent != null && em != null)
        {
            object paramRef = null;
            onEvent(em, ref paramRef);
        }
    }

    public void Send(EventMessage em, ref object paramRef)
    {
        if (onEvent != null && em != null)
        {
            onEvent(em, ref paramRef);
        }
    }
}