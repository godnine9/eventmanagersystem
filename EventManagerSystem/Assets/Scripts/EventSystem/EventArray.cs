using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

[System.Serializable]
public class EventElement
{
    [SenderEventIdAttribute]
    public string eventID;
    public List<GameObject> eventTarget;
    public bool sendToSelf = false;
    public bool paramBool;
    public string paramString;
    public Object paramObj;
    public Vector2 delayTime;
    public bool useTimeScale;
    public string completeCondition;

    public EventElement()
    {
        paramBool = true;
        useTimeScale = true;
    }

    public void Send(GameObject origSource, object sourceParam)
    {
        EventManager.CompleteCondition(completeCondition);

        if (string.IsNullOrEmpty(eventID))
        {
            return;
        }

        // Make extra parameter
        object[] paramEX = null;
        if (sourceParam != null || paramObj != null)
        {
            paramEX = new object[] { sourceParam, paramObj };
        }

        if (sendToSelf)
        {
            EventManager.SendObjectEvent(origSource, eventID, paramBool, paramString, origSource, delayTime, useTimeScale, paramEX);
        }
        else if (eventTarget == null || eventTarget.Count <= 0)
        {
            // Global Event
            EventManager.SendGlobalEvent(eventID, paramBool, paramString, origSource, delayTime, useTimeScale, paramEX);
        }
        else
        {
            // Object Event
            foreach (GameObject go in eventTarget)
            {
                if (go != null)
                {
                    EventManager.SendObjectEvent(go, eventID, paramBool, paramString, origSource, delayTime, useTimeScale, paramEX);
                }
                else
                {
                    // When someone missing some target in eventTarget
                    Debug.LogWarning("EventArray: null object in eventTarget. " + origSource + " eventID = " + eventID);
                }
            }
        }
    }

    static GameObject eventDummy;

    /// <summary>
    /// Gets the game object.
    /// </summary>
    /// <returns>
    /// The game object.
    /// </returns>
    /// <param name='obj'>
    /// Object.
    /// </param>
    static public GameObject GetGameObject(Object obj)
    {
        GameObject go = eventDummy;
        if (obj != null)
        {
#if !UNITY_EDITOR && UNITY_WSA_10_0__   // shao: il2cpp win 10 using same code...
				if (obj.GetType().GetTypeInfo().IsSubclassOf(typeof(Component)))
				{
				Component comp = obj as Component;
				if (comp != null)
				{
				go = comp.gameObject;
				}
				}
				else if (obj.GetType() == typeof(GameObject) || obj.GetType().GetTypeInfo().IsSubclassOf(typeof(GameObject)))
				{
				GameObject gameObj = obj as GameObject;
				if (gameObj != null)
				{
				go = gameObj;
				}
				}
#else
            if (obj.GetType().IsSubclassOf(typeof(Component)))
            {
                Component comp = obj as Component;
                if (comp != null)
                {
                    go = comp.gameObject;
                }
            }
            else if (obj.GetType() == typeof(GameObject) || obj.GetType().IsSubclassOf(typeof(GameObject)))
            {
                GameObject gameObj = obj as GameObject;
                if (gameObj != null)
                {
                    go = gameObj;
                }
            }
#endif
        }

        if (go == null)
        {
            eventDummy = go = new GameObject("BadBird.EventElement.Dummy");
            GameObject.DontDestroyOnLoad(go);
            go.hideFlags = HideFlags.HideAndDontSave;
        }
        return go;
    }
}

/// <summary>
/// Event array.
/// </summary>
[System.Serializable]
public class EventArray
{
    //[Auto]
    public List<EventElement> eventArray = new List<EventElement>();
    [HideInInspector]
    public bool isDisplay = true;

    public void Broadcast(GameObject sourceObj, object sourceParam)
    {
        foreach (EventElement ee in eventArray)
        {
            ee.Send(sourceObj, sourceParam);
        }
    }

    public static EventArray operator +(EventArray left, EventArray right)
    {
        EventArray ret = new EventArray();
        ret.eventArray.AddRange(left.eventArray);
        ret.eventArray.AddRange(right.eventArray);
        return ret;
    }
}
