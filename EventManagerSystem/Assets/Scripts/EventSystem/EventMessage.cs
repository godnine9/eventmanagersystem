using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EventMessage
{
    public string eventID;
    public Object origSource;
    public bool paramBool = true;
    public string paramString;
    public object[] paramEX;
    public Vector2 delayTime = Vector2.zero;
    public bool useTimeScale = true;
    public GameObject targetObj; // Object event only

    public EventMessage(
        string eventID,
        Object origSource = null,
        bool paramBool = false,
        string paramString = null,
        object[] paramEX = null,
        Vector2 delayTime = default(Vector2),
        bool useTimeScale = true)
    {
        this.eventID = eventID;
        this.origSource = origSource;
        this.paramBool = paramBool;
        this.paramString = paramString;
        this.paramEX = paramEX;
        this.delayTime = delayTime;
        this.useTimeScale = useTimeScale;
    }

    // Object event only
    public EventMessage(
        GameObject targetObj,
        string eventID = null,
        Object origSource = null,
        bool paramBool = false,
        string paramString = null,
        object[] paramEX = null,
        Vector2 delayTime = default(Vector2),
        bool useTimeScale = false) :
    this(
        eventID,
        origSource,
        paramBool,
        paramString,
        paramEX,
        delayTime,
        useTimeScale)
    {
        this.targetObj = targetObj;
    }
}