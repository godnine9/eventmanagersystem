using UnityEngine;
using System.Collections.Generic;
using System.Collections.ObjectModel;

public class EmptyEventLogger : EventLogger
{
    override public void Clear()
    {

    }

    override public void AddSourceEntry(EventMessage message, UnityEngine.Object source)
    {

    }

    override public void AddListenerEntry(EventMessage message, UnityEngine.Object listener)
    {

    }

    override public StringCollection GetCollection()
    {
        return new StringCollection(new Parameter());
    }

    override public EntryDetail GetDetail(string text)
    {
        return new EntryDetail();
    }

    override public bool IsSourceEntry(string text)
    {
        return true;
    }
}