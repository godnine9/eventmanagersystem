using System.Collections.Generic;

public class EventManagerBase // doesnt like to be inherited
{
    Dictionary<string, EventInfo> eventDictionary = new Dictionary<string, EventInfo>();

    public Dictionary<string, EventInfo> EventDictionary
    {
        get
        {
            return eventDictionary;
        }
    }

    public void AddListener(string eventID, EventFunction ed)
    {
        EventInfo ei = GetOrCreateEventInfo(eventID);
        if (ei != null)
        {
            ei.AddDelegate(ed);
        }
    }

    public void RemoveListener(string eventID, EventFunction ed)
    {
        EventInfo ei = GetEventInfo(eventID);
        if (ei != null)
        {
            ei.RemoveDelegate(ed);
        }
    }

    public void Send(EventMessage em)
    {
        if (em == null)
        {
            return;
        }

        object paramRef = null;
        Send(em, ref paramRef);
    }

    private void Send(EventMessage em, ref object paramRef)
    {
        if (em == null)
        {
            return;
        }

        em.delayTime[0] = UnityEngine.Mathf.Max(0.0f, em.delayTime[0]);
        em.delayTime[1] = UnityEngine.Mathf.Max(0.0f, em.delayTime[1]);

        if (em.delayTime[0] > 0 || em.delayTime[1] > 0)
        {
            // ignore paramRef
            EventTimer.AddTimer(em, SendNoDelay);
        }
        else
        {
            SendNoDelay(em, ref paramRef);
        }
    }

    private void SendNoDelay(EventMessage em, ref object paramRef)
    {
        EventInfo ei = GetEventInfo(em.eventID);
        if (ei == null)
        {
            return;
        }

        AddEventLog(em);
        ei.Send(em, ref paramRef);
    }

    private EventInfo GetOrCreateEventInfo(string eventID)
    {
        if (string.IsNullOrEmpty(eventID))
        {
            return null;
        }

        EventInfo retEventInfo;
        if (eventDictionary.TryGetValue(eventID, out retEventInfo))
        {
            return retEventInfo;
        }
        else
        {
            eventDictionary[eventID] = new EventInfo(eventID);
            return eventDictionary[eventID];
        }
    }

    private EventInfo GetEventInfo(string eventID)
    {
        EventInfo retEventInfo;
        if (eventDictionary.TryGetValue(eventID, out retEventInfo))
        {
            return retEventInfo;
        }
        return null;
    }

    private void AddEventLog(EventMessage em)
    {
        EventLogCollection.AddEntryForSource(em, em.origSource);
    }

}