using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EventToActivate : SwitchEventListener
{
    public GameObject[] Targets;
    public bool IsActive;

    public bool setRecursive = true;


    protected override void OnEvent(EventMessage message, ref object paramRef)
    {
        for(int aIndex = 0;aIndex < Targets.Length; aIndex++)
        {
            Debug.Log(Targets[aIndex].name);
            Targets[aIndex].SetActive(IsActive);
        }
    }
}